/*
Cache Simulator
Level one L1 and level two L2 cache parameters are read from file (block size, line per set and set per cache).
The 32 bit address is divided into tag bits (t), set index bits (s) and block offset bits (b)
s = log2(#sets)   b = log2(block size)  t=32-s-b
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <stdlib.h>
#include <cmath>
#include <bitset>

using namespace std;
//access state:
#define NA 0 // no action
#define RH 1 // read hit
#define RM 2 // read miss
#define WH 3 // Write hit
#define WM 4 // write miss




struct config{
       int L1blocksize;
       int L1setsize;
       int L1size;
       int L2blocksize;
       int L2setsize;
       int L2size;
       };

struct number_of_bits{
    int tag_L1;
    int index_L1;
    int offset_L1;
    int tag_L2;
    int index_L2;
    int offset_L2;
};
/* you can define the cache class here, or design your own data structure for L1 and L2 cache
class cache {

      }
*/

class Cache_Config {
public:
    vector<vector<vector <int> > > l1;
    vector<vector<vector <int> > > l2;
    vector <int> l1_counter;
    vector <int> l2_counter;
    int l1_counter_way, l2_counter_way;

    vector <int> accesses;

    void cache_configuration(int block_size, int set_size, int total_size, int cache_select){
        int total_blocks;

        if(set_size > 0){
            total_blocks = ((total_size * 1024)/block_size)/set_size;
        } else {
            total_blocks = 1;
            set_size = ((total_size * 1024)/block_size);
        }
        if(cache_select == 1){
            for(int i = 0; i < set_size; i++){
                l1.push_back(vector<vector<int> >());
                for(int j = 0; j < total_blocks; j++){
                    l1[i].push_back(vector<int>());
                    l1_counter.push_back(0);
                    for(int k = 0; k < block_size; k++){
                        l1[i][j].push_back(999);
                    }
                }
            }
            l1_counter_way = set_size;
        } else {
            for(int i = 0; i < set_size; i++){
                l2.push_back(vector<vector<int> >());
                for(int j = 0; j < total_blocks; j++){
                    l2[i].push_back(vector<int>());
                    l2_counter.push_back(0);
                    for(int k = 0; k < block_size; k++){
                        l2[i][j].push_back(999);
                    }
                }
            }
            l2_counter_way = set_size;
        }
    }

    //L1 Cache
    void read(number_of_bits nob, bitset<32> addr, int cache_select){
        int hit_flag = 0;
        int addr_int = addr.to_ulong();
        if(cache_select == 1){
            int tag = strtol((addr.to_string().substr(0, nob.tag_L1)).c_str(), NULL, 2);
            int index = strtol((addr.to_string().substr(nob.tag_L1, nob.index_L1)).c_str(), NULL, 2);
            int offset = strtol((addr.to_string().substr(nob.tag_L1 + nob.index_L1)).c_str(), NULL, 2);

            for(int i = 0; i < l1.size(); i++){
                if(l1[i][index][offset] == addr_int){
                    //READ HIT IN L1

                    accesses.push_back(RH);
                    accesses.push_back(NA);
                    hit_flag = 1;
                    break;
                } else {
                }
            }

            if(hit_flag == 0){
                //READ MISS IN L1
                accesses.push_back(RM);
                read(nob, addr, 2);
            }

        } else {
            int tag = strtol((addr.to_string().substr(0, nob.tag_L2)).c_str(), NULL, 2);
            int index = strtol((addr.to_string().substr(nob.tag_L2, nob.index_L2)).c_str(), NULL, 2);
            int offset = strtol((addr.to_string().substr(nob.tag_L2 + nob.index_L2)).c_str(), NULL, 2);

            for(int i = 0; i < l2.size(); i++){
                if(l2[i][index][offset] == addr_int){
                    //READ HIT IN L2
                    accesses.push_back(RH);
                    //WRITE IT BACK TO L1 (I THINK)
                    write(nob, addr, 1, 1);

                    hit_flag = 1;
                    break;
                }
            }

            if(hit_flag == 0){
                //READ MISS IN L2 i.e THE MOST COMPLEX SCENARIO PART
                accesses.push_back(RM);
                //SEND TO WRITE AND ALL
                write(nob, addr, 2, 1);
                write(nob, addr, 1, 1);
            }
        }
    }

    void write(number_of_bits nob, bitset<32> addr, int cache_select, int is_read){
        int hit_flag = 0;
        int addr_int = addr.to_ulong();
        if(cache_select == 1){
            int tag = strtol((addr.to_string().substr(0, nob.tag_L1)).c_str(), NULL, 2);
            int index = strtol((addr.to_string().substr(nob.tag_L1, nob.index_L1)).c_str(), NULL, 2);
            int offset = strtol((addr.to_string().substr(nob.tag_L1 + nob.index_L1)).c_str(), NULL, 2);

            for(int i = 0; i < l1.size(); i++){
                if(l1[i][index][offset] == addr_int){
                    //WRITE HIT IN L1
                    accesses.push_back(WH);
                    accesses.push_back(NA);
                    hit_flag = 1;
                    break;
                }
            }

            if(hit_flag == 0){
                //WRITE MISS IN L1
                accesses.push_back(WM);
                int to_evict = 1;
                //EVICT BLOCK AND SEND TO L2 AND UPDATE THE BLOCK WITH CURRENT REQUEST
                //EVICT ONLY IF THERE IS NO SPACE

                if(is_read == 1){
                    for(int i = 0; i < l1.size(); i++){
                        if(l1[i][index][offset] == 999){
                            l1[i][index][offset] = addr.to_ulong();

                            to_evict = 0;
                            break;
                        }
                    }

                    if(to_evict == 1){
                        bitset<32> evict = (bitset<32>)l1[ l1_counter[index] ][index][offset];
                        l1[ l1_counter[index] ][index][offset] = addr.to_ulong();
                        l1_counter[index]++;
                        l1_counter[index] %= l1_counter_way;
                        write(nob, evict, 2, 0);
                    }
                } else {
                    write(nob, addr, 2, 0);
                }
            }
        } else {
            int tag = strtol((addr.to_string().substr(0, nob.tag_L2)).c_str(), NULL, 2);
            int index = strtol((addr.to_string().substr(nob.tag_L2, nob.index_L2)).c_str(), NULL, 2);
            int offset = strtol((addr.to_string().substr(nob.tag_L2 + nob.index_L2)).c_str(), NULL, 2);

            for(int i = 0; i < l2.size(); i++){
                if(l2[i][index][offset] == addr_int){
                    //WRITE HIT IN L2
                    accesses.push_back(WH);
                    hit_flag = 1;
                    break;
                }
            }

            if(hit_flag == 0){
                //WRITE MISS IN L2
                accesses.push_back(WM);
                int to_evict = 1;
                int i;
                if(is_read == 1){

                    for(i = 0; i < l2.size(); i++){
                        if(l2[i][index][offset] == 999){
                            to_evict = 0;
                            break;
                        }
                    }

                    if(to_evict == 0){
                        for(int j = 0; j < l2[0][0].size(); j++){
                            bitset<32> offset_val (j);
                            bitset<32> test (addr.to_string().substr(0, nob.tag_L2) +
                                        addr.to_string().substr(nob.tag_L2, nob.index_L2) +
                                        offset_val.to_string().substr(nob.tag_L2 + nob.index_L2));
                            l2[i][index][j] = test.to_ulong();
                        }
                    } else {
                        i = l2_counter[index];
                        for(int j = 0; j < l2[0][0].size(); j++){
                            bitset<32> offset_val (j);
                            bitset<32> test (addr.to_string().substr(0, nob.tag_L2) +
                                        addr.to_string().substr(nob.tag_L2, nob.index_L2) +
                                        offset_val.to_string().substr(nob.tag_L2 + nob.index_L2));

                            l2[i][index][j] = test.to_ulong();
                        }
                    }
                }
            }
        }
    }
};

int main(int argc, char* argv[]){

    number_of_bits address_bits;
    config cacheconfig;
    ifstream cache_params;
    string dummyLine;
    cache_params.open(argv[1]);
    while(!cache_params.eof())  // read config file
    {
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L1blocksize;
      cache_params>>cacheconfig.L1setsize;
      cache_params>>cacheconfig.L1size;
      cache_params>>dummyLine;
      cache_params>>cacheconfig.L2blocksize;
      cache_params>>cacheconfig.L2setsize;
      cache_params>>cacheconfig.L2size;
      }



   // Implement by you:
   // initialize the hirearch cache system with those configs
   // probably you may define a Cache class for L1 and L2, or any data structure you like

   //CACHE DATA STRUCTURE: cache.(l1/l2)[way][index][block_offset]
   Cache_Config cache;
   //For L1
   cache.cache_configuration(cacheconfig.L1blocksize, cacheconfig.L1setsize, cacheconfig.L1size, 1);

   //For L2
   cache.cache_configuration(cacheconfig.L2blocksize, cacheconfig.L2setsize, cacheconfig.L2size, 2);
   address_bits.index_L1 = log2(cache.l1[0].size());
   address_bits.offset_L1 = log2(cacheconfig.L1blocksize);
   address_bits.tag_L1 = 32 - address_bits.index_L1 - address_bits.offset_L1;

   address_bits.index_L2 = log2(cache.l2[0].size());
   address_bits.offset_L2 = log2(cacheconfig.L2blocksize);
   address_bits.tag_L2 = 32 - address_bits.index_L2 - address_bits.offset_L2;


  int L1AcceState = 0; // L1 access state variable, can be one of NA, RH, RM, WH, WM;
  int L2AcceState = 0; // L2 access state variable, can be one of NA, RH, RM, WH, WM;


    ifstream traces;
    ofstream tracesout;
    string outname;
    outname = string(argv[2]) + ".out";

    traces.open(argv[2]);
    tracesout.open(outname.c_str());

    string line;
    string accesstype;  // the Read/Write access type from the memory trace;
    string xaddr;       // the address from the memory trace store in hex;
    unsigned int addr;  // the address from the memory trace store in unsigned int;
    bitset<32> accessaddr; // the address from the memory trace store in the bitset;
    int counter = 0;


    if (traces.is_open()&&tracesout.is_open()){
        while (getline (traces,line)){   // read mem access file and access Cache

            istringstream iss(line);
            if (!(iss >> accesstype >> xaddr)) {break;}
            stringstream saddr(xaddr);
            saddr >> std::hex >> addr;
            accessaddr = bitset<32> (addr);

           // access the L1 and L2 Cache according to the trace;
              if (accesstype.compare("R")==0)

             {
                 //Implement by you:
                 // read access to the L1 Cache,
                 //  and then L2 (if required),
                 //  update the L1 and L2 access state variable;

                 cache.read(address_bits, accessaddr, 1);
                 L1AcceState = cache.accesses[0];
                 L2AcceState = cache.accesses[1];
                 cache.accesses.clear();

                 }
             else
             {
                   //Implement by you:
                  // write access to the L1 Cache,
                  //and then L2 (if required),
                  //update the L1 and L2 access state variable;

                  cache.write(address_bits, accessaddr, 1, 0);
                  L1AcceState = cache.accesses[0];
                  L2AcceState = cache.accesses[1];
                  cache.accesses.clear();


                  }



            tracesout<< L1AcceState << " " << L2AcceState << endl;  // Output hit/miss results for L1 and L2 to the output file;


        }
        traces.close();
        tracesout.close();
    }
    else cout<< "Unable to open trace or traceout file ";



    return 0;
}
